-- PRIMERA CONSULTA #########################################
SELECT
    cl.id AS "ID Cliente",
    CONCAT(cl.nombres, ' ', cl.apellidos) AS "Nombre Cliente",
	cl.razon AS "Razon Social",
	COUNT(CASE WHEN cu.es_activa THEN 1 END) AS "No. Ctas. Activas",
    COUNT(CASE WHEN cu.es_activa = false THEN 1 END) AS "No. Ctas. Desactivadas",
	COUNT(cu.id) AS "Total de Cuentas"
FROM cliente cl
LEFT JOIN cuenta cu ON cu.cliente_id = cl.id
GROUP BY cl.id;

-- SEGUNDA CONSULTA #########################################
SELECT 
	EXTRACT(YEAR FROM fecha) AS "Año",
	EXTRACT(MONTH FROM fecha) AS "Mes",
    SUM(
		CASE 
            WHEN es_deposito THEN monto
			ELSE -monto
        END
	) AS "Saldo Mensual"
FROM transaccion
GROUP BY EXTRACT(MONTH FROM fecha), EXTRACT(YEAR FROM fecha)
ORDER BY "Año", "Mes";

-- TERCERA CONSULTA #########################################
SELECT 
	cl.id AS "ID Cliente", 
	CONCAT(cl.nombres, ' ', cl.apellidos) AS "Nombre", 
	cl.razon AS "Razon Social",
FROM cliente cl
JOIN cuenta cu ON cu.cliente_id = cl.id 
JOIN transaccion t ON t.cuenta_id = cu.id 
WHERE cu.saldo > 20000
  AND t.es_deposito = true
  AND t.fecha >= NOW() - INTERVAL '15 days'
GROUP BY cl.id;

-- CUARTA CONSULTA #########################################
SELECT  
	a.nombre AS "Nombre Agencia", 
	a.no_empleados AS "No Empleados"
FROM cliente cl
JOIN cuenta cu ON cu.cliente_id = cl.id  
JOIN transaccion t ON t.cuenta_id = cu.id
JOIN agencia a ON  a.id = t.agencia_id
WHERE t.es_deposito = true
  AND t.fecha >= NOW() - INTERVAL '5 days'
GROUP BY a.id;

-- QUINTA CONSULTA #########################################
SELECT 
		EXTRACT(MONTH FROM t.fecha) AS "Mes",
	   	cl.id AS "ID Cliente", 
		CONCAT(cl.nombres, ' ', cL.apellidos) AS "Nombre"
FROM cliente cl
JOIN cuenta cu ON cu.cliente_id = cl.id  
JOIN transaccion t ON t.cuenta_id = cu.id
WHERE 
	(t.monto > 2000 AND t.moneda = '$' AND cu.moneda = 'Q') OR
	(t.monto > 20000 AND t.moneda = 'Q')
GROUP BY cl.id, "Mes"
UNION DISTINCT
SELECT 
		EXTRACT(MONTH FROM t.fecha) AS "Mes",
	   	cl.id AS "ID Cliente", 
		CONCAT(cl.nombres, ' ', cL.apellidos) AS "Nombre"
FROM cliente cl
JOIN cuenta cu ON cu.cliente_id = cl.id  
JOIN transaccion t ON t.cuenta_id = cu.id
GROUP BY cl.id, "Mes"
HAVING 
	COUNT(DISTINCT t.agencia_id) > 10 AND
	cl.id NOT IN (
    	SELECT cu.cliente_id
    	FROM transaccion t
		JOIN agencia a ON a.id = t.agencia_id
		JOIN cuenta cu ON cu.id = t.cuenta_id 
    	WHERE t.es_deposito = true AND a.es_central = true
	)
;
	