<?php

namespace App\Entity;

use App\Repository\ClienteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\Municipio;
use App\Entity\Documento;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;

#[ORM\Entity(repositoryClass: ClienteRepository::class)]
#[ORM\UniqueConstraint(name: 'UNIQ_IDENTIFIER_USERNAME', fields: ['username'])]
class Cliente implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 180)]
    private ?string $username = null;

    /**
     * @var list<string> The user roles
     */
    #[ORM\Column]
    private array $roles = [];

    #[ORM\Column(length: 30, nullable: true)]
    private ?string $nombres = null;

    #[ORM\Column(length: 30, nullable: true)]
    private ?string $apellidos = null;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $razon = null;

    #[ORM\ManyToOne(inversedBy: 'clientes')]
    #[ORM\JoinColumn(nullable: false)]
    private ?municipio $municipio = null;

    #[ORM\ManyToOne(inversedBy: 'clientes')]
    #[ORM\JoinColumn(nullable: false)]
    private ?documento $documento = null;

    #[ORM\Column(length: 30)]
    private ?string $password = null;

    /**
     * @var Collection<int, Cuenta>
     */
    #[ORM\OneToMany(targetEntity: Cuenta::class, mappedBy: 'cliente', orphanRemoval: true)]
    private Collection $cuentas;

    public function __construct()
    {
        $this->cuentas = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): static
    {
        $this->username = $username;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->username;
    }

    /**
     * @see UserInterface
     *
     * @return list<string>
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @param list<string> $roles
     */
    public function setRoles(array $roles): static
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getNombres(): ?string
    {
        return $this->nombres;
    }

    public function setNombres(?string $nombres): static
    {
        $this->nombres = $nombres;

        return $this;
    }

    public function getApellidos(): ?string
    {
        return $this->apellidos;
    }

    public function setApellidos(?string $apellidos): static
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    public function getRazon(): ?string
    {
        return $this->razon;
    }

    public function setRazon(?string $razon): static
    {
        $this->razon = $razon;

        return $this;
    }

    public function getMunicipio(): ?municipio
    {
        return $this->municipio;
    }

    public function setMunicipio(?municipio $municipio): static
    {
        $this->municipio = $municipio;

        return $this;
    }

    public function getDocumento(): ?documento
    {
        return $this->documento;
    }

    public function setDocumento(?documento $documento): static
    {
        $this->documento = $documento;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return Collection<int, Cuenta>
     */
    public function getCuentas(): Collection
    {
        return $this->cuentas;
    }

    public function addCuenta(Cuenta $cuenta): static
    {
        if (!$this->cuentas->contains($cuenta)) {
            $this->cuentas->add($cuenta);
            $cuenta->setCliente($this);
        }

        return $this;
    }

    public function removeCuenta(Cuenta $cuenta): static
    {
        if ($this->cuentas->removeElement($cuenta)) {
            // set the owning side to null (unless already changed)
            if ($cuenta->getCliente() === $this) {
                $cuenta->setCliente(null);
            }
        }

        return $this;
    }
}
