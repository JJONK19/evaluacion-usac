<?php

namespace App\Entity;

use App\Repository\TransaccionRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TransaccionRepository::class)]
class Transaccion
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50)]
    private ?string $descripcion = null;

    #[ORM\ManyToOne(inversedBy: 'transaccions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?agencia $agencia = null;

    #[ORM\ManyToOne(inversedBy: 'transaccions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?cuenta $cuenta = null;

    #[ORM\Column(length: 1)]
    private ?string $moneda = null;

    #[ORM\Column]
    private ?int $monto = null;

    #[ORM\Column]
    private ?bool $esDeposito = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $fecha = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): static
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getAgencia(): ?agencia
    {
        return $this->agencia;
    }

    public function setAgencia(?agencia $agencia): static
    {
        $this->agencia = $agencia;

        return $this;
    }

    public function getCuenta(): ?cuenta
    {
        return $this->cuenta;
    }

    public function setCuenta(?cuenta $cuenta): static
    {
        $this->cuenta = $cuenta;

        return $this;
    }

    public function getMoneda(): ?string
    {
        return $this->moneda;
    }

    public function setMoneda(string $moneda): static
    {
        $this->moneda = $moneda;

        return $this;
    }

    public function getMonto(): ?int
    {
        return $this->monto;
    }

    public function setMonto(int $monto): static
    {
        $this->monto = $monto;

        return $this;
    }

    public function isEsDeposito(): ?bool
    {
        return $this->esDeposito;
    }

    public function setEsDeposito(bool $esDeposito): static
    {
        $this->esDeposito = $esDeposito;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): static
    {
        $this->fecha = $fecha;

        return $this;
    }
}
