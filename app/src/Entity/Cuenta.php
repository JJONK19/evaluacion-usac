<?php

namespace App\Entity;

use App\Repository\CuentaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CuentaRepository::class)]
class Cuenta
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'cuentas')]
    #[ORM\JoinColumn(nullable: false)]
    private ?cliente $cliente = null;

    #[ORM\Column]
    private ?bool $esActiva = null;

    #[ORM\Column]
    private ?int $saldo = null;

    #[ORM\Column(length: 1)]
    private ?string $moneda = null;

    /**
     * @var Collection<int, Transaccion>
     */
    #[ORM\OneToMany(targetEntity: Transaccion::class, mappedBy: 'cuenta', orphanRemoval: true)]
    private Collection $transaccions;

    public function __construct()
    {
        $this->transaccions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCliente(): ?cliente
    {
        return $this->cliente;
    }

    public function setCliente(?cliente $cliente): static
    {
        $this->cliente = $cliente;

        return $this;
    }

    public function isEsActiva(): ?bool
    {
        return $this->esActiva;
    }

    public function setEsActiva(bool $esActiva): static
    {
        $this->esActiva = $esActiva;

        return $this;
    }

    public function getSaldo(): ?int
    {
        return $this->saldo;
    }

    public function setSaldo(int $saldo): static
    {
        $this->saldo = $saldo;

        return $this;
    }

    public function getMoneda(): ?string
    {
        return $this->moneda;
    }

    public function setMoneda(string $moneda): static
    {
        $this->moneda = $moneda;

        return $this;
    }

    /**
     * @return Collection<int, Transaccion>
     */
    public function getTransaccions(): Collection
    {
        return $this->transaccions;
    }

    public function addTransaccion(Transaccion $transaccion): static
    {
        if (!$this->transaccions->contains($transaccion)) {
            $this->transaccions->add($transaccion);
            $transaccion->setCuenta($this);
        }

        return $this;
    }

    public function removeTransaccion(Transaccion $transaccion): static
    {
        if ($this->transaccions->removeElement($transaccion)) {
            // set the owning side to null (unless already changed)
            if ($transaccion->getCuenta() === $this) {
                $transaccion->setCuenta(null);
            }
        }

        return $this;
    }
}
