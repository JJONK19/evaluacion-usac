<?php

namespace App\Entity;

use App\Repository\AgenciaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AgenciaRepository::class)]
class Agencia
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 25)]
    private ?string $nombre = null;

    #[ORM\Column]
    private ?bool $esCentral = null;

    #[ORM\Column]
    private ?int $noEmpleados = null;

    #[ORM\ManyToOne(inversedBy: 'agencias')]
    private ?municipio $municipio = null;

    /**
     * @var Collection<int, Empleado>
     */
    #[ORM\OneToMany(targetEntity: Empleado::class, mappedBy: 'agencia', orphanRemoval: true)]
    private Collection $empleados;

    /**
     * @var Collection<int, Transaccion>
     */
    #[ORM\OneToMany(targetEntity: Transaccion::class, mappedBy: 'agencia', orphanRemoval: true)]
    private Collection $transaccions;

    public function __construct()
    {
        $this->empleados = new ArrayCollection();
        $this->transaccions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): static
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function isEsCentral(): ?bool
    {
        return $this->esCentral;
    }

    public function setEsCentral(bool $esCentral): static
    {
        $this->esCentral = $esCentral;

        return $this;
    }

    public function getNoEmpleados(): ?int
    {
        return $this->noEmpleados;
    }

    public function setNoEmpleados(int $noEmpleados): static
    {
        $this->noEmpleados = $noEmpleados;

        return $this;
    }

    public function getMunicipio(): ?municipio
    {
        return $this->municipio;
    }

    public function setMunicipio(?municipio $municipio): static
    {
        $this->municipio = $municipio;

        return $this;
    }

    /**
     * @return Collection<int, Empleado>
     */
    public function getEmpleados(): Collection
    {
        return $this->empleados;
    }

    public function addEmpleado(Empleado $empleado): static
    {
        if (!$this->empleados->contains($empleado)) {
            $this->empleados->add($empleado);
            $empleado->setAgencia($this);
        }

        return $this;
    }

    public function removeEmpleado(Empleado $empleado): static
    {
        if ($this->empleados->removeElement($empleado)) {
            // set the owning side to null (unless already changed)
            if ($empleado->getAgencia() === $this) {
                $empleado->setAgencia(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Transaccion>
     */
    public function getTransaccions(): Collection
    {
        return $this->transaccions;
    }

    public function addTransaccion(Transaccion $transaccion): static
    {
        if (!$this->transaccions->contains($transaccion)) {
            $this->transaccions->add($transaccion);
            $transaccion->setAgencia($this);
        }

        return $this;
    }

    public function removeTransaccion(Transaccion $transaccion): static
    {
        if ($this->transaccions->removeElement($transaccion)) {
            // set the owning side to null (unless already changed)
            if ($transaccion->getAgencia() === $this) {
                $transaccion->setAgencia(null);
            }
        }

        return $this;
    }
}
