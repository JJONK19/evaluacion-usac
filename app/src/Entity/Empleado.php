<?php

namespace App\Entity;

use App\Repository\EmpleadoRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EmpleadoRepository::class)]
class Empleado
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 30)]
    private ?string $nombres = null;

    #[ORM\Column(length: 30)]
    private ?string $apellidos = null;

    #[ORM\ManyToOne(inversedBy: 'empleados')]
    #[ORM\JoinColumn(nullable: false)]
    private ?agencia $agencia = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombres(): ?string
    {
        return $this->nombres;
    }

    public function setNombres(string $nombres): static
    {
        $this->nombres = $nombres;

        return $this;
    }

    public function getApellidos(): ?string
    {
        return $this->apellidos;
    }

    public function setApellidos(string $apellidos): static
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    public function getAgencia(): ?agencia
    {
        return $this->agencia;
    }

    public function setAgencia(?agencia $agencia): static
    {
        $this->agencia = $agencia;

        return $this;
    }
}
