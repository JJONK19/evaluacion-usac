<?php

namespace App\Entity;

use App\Repository\MunicipioRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Departamento;

#[ORM\Entity(repositoryClass: MunicipioRepository::class)]
class Municipio
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 25)]
    private ?string $nombre = null;

    #[ORM\ManyToOne(inversedBy: 'municipios')]
    #[ORM\JoinColumn(nullable: false)]
    private ?departamento $departamento = null;

    /**
     * @var Collection<int, Cliente>
     */
    #[ORM\OneToMany(targetEntity: Cliente::class, mappedBy: 'municipio', orphanRemoval: true)]
    private Collection $clientes;

    /**
     * @var Collection<int, Agencia>
     */
    #[ORM\OneToMany(targetEntity: Agencia::class, mappedBy: 'municipio')]
    private Collection $agencias;

    public function __construct()
    {
        $this->clientes = new ArrayCollection();
        $this->agencias = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): static
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getDepartamento(): ?departamento
    {
        return $this->departamento;
    }

    public function setDepartamento(?departamento $departamento): static
    {
        $this->departamento = $departamento;

        return $this;
    }

    /**
     * @return Collection<int, Cliente>
     */
    public function getClientes(): Collection
    {
        return $this->clientes;
    }

    public function addCliente(Cliente $cliente): static
    {
        if (!$this->clientes->contains($cliente)) {
            $this->clientes->add($cliente);
            $cliente->setMunicipio($this);
        }

        return $this;
    }

    public function removeCliente(Cliente $cliente): static
    {
        if ($this->clientes->removeElement($cliente)) {
            // set the owning side to null (unless already changed)
            if ($cliente->getMunicipio() === $this) {
                $cliente->setMunicipio(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Agencia>
     */
    public function getAgencias(): Collection
    {
        return $this->agencias;
    }

    public function addAgencia(Agencia $agencia): static
    {
        if (!$this->agencias->contains($agencia)) {
            $this->agencias->add($agencia);
            $agencia->setMunicipio($this);
        }

        return $this;
    }

    public function removeAgencia(Agencia $agencia): static
    {
        if ($this->agencias->removeElement($agencia)) {
            // set the owning side to null (unless already changed)
            if ($agencia->getMunicipio() === $this) {
                $agencia->setMunicipio(null);
            }
        }

        return $this;
    }
}
