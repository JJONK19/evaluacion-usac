--	LLENAR LOS DEPARTAMENTOS
INSERT INTO departamento(nombre) VALUES ('Guatemala');
INSERT INTO departamento(nombre) VALUES ('Huehuetenango');
-- SELECT * FROM departamento;

-- LLENAR LOS TIPOS DE DOCUMENTO
INSERT INTO documento(tipo) VALUES ('Pasaporte');
INSERT INTO documento(tipo) VALUES ('DPI');
-- SELECT * FROM documento;

-- LLENAR LOS MUNICIPIOS
INSERT INTO municipio(departamento_id, nombre) VALUES (1, 'Guatemala');
INSERT INTO municipio(departamento_id, nombre) VALUES (1, 'Mixco');
INSERT INTO municipio(departamento_id, nombre) VALUES (1, 'Villa Nueva');
-- SELECT * FROM municipio;

-- AÑADIR LOS CLIENTES DEFAULT
INSERT INTO cliente(username, municipio_id, documento_id, roles, nombres, apellidos, razon, password)
	VALUES ('5263987852369', 1, 2, json_build_array('ROLE_USER'),'Juan Alfonso', 'Perez Castos', NULL, '12345');
INSERT INTO cliente(username, municipio_id, documento_id, roles, nombres, apellidos, razon, password)
	VALUES ('2687635125698', 2, 2, json_build_array('ROLE_USER'),'Pedro Ricardo', 'Lopez Lopez', NULL, '12345');
INSERT INTO cliente(username, municipio_id, documento_id, roles, nombres, apellidos, razon, password)
	VALUES ('9003356A', 3, 1, json_build_array('ROLE_USER'),NULL, NULL, 'Los pollos hermanos', '12345');
INSERT INTO cliente(username, municipio_id, documento_id, roles, nombres, apellidos, razon, password)
	VALUES ('9999999A', 1, 1, json_build_array('ROLE_USER'),NULL, NULL, 'Tienda la Bendicion', '12345');
-- SELECT * FROM cliente;

-- LLENAR LAS AGENCIAS
INSERT INTO agencia(municipio_id, nombre, es_central, no_empleados) 
	VALUES (1, 'Zona 1', true, 0);
INSERT INTO agencia(municipio_id, nombre, es_central, no_empleados) 
	VALUES (2, 'Primero Julio', false, 0);
INSERT INTO agencia(municipio_id, nombre, es_central, no_empleados) 
	VALUES (3, 'Municipalidad', false, 0);
-- SELECT * FROM agencia;

-- LLENAR LOS EMPLEADOS
CALL crearEmpleado(1, 'Javier', 'Aguilar');
CALL crearEmpleado(1, 'Josue', 'Lopez');
CALL crearEmpleado(2, 'Nicole', 'Perez');
CALL crearEmpleado(3, 'Maria', 'Molina');
CALL crearEmpleado(3, 'Carlos', 'Ortega');
CALL crearEmpleado(3, 'Maria', 'Davila');
-- SELECT * FROM empleado;

-- CREAR CUENTA
CALL crearCuenta(4, 'Q');
CALL crearCuenta(4, '$');
-- SELECT * FROM cuenta;

-- CREAR TRANSACCIONES
CALL transaccion(1, 1, 'Pago de Salarios', 'Q', 25000, 'D'); -- Deuda
CALL transaccion(1, 1, 'Deuda Mueble', 'Q', 1000, 'R'); -- Retiro
-- SELECT * FROM transaccion;