--
-- PostgreSQL database dump
--

-- Dumped from database version 14.11 (Ubuntu 14.11-0ubuntu0.22.04.1)
-- Dumped by pg_dump version 14.11 (Ubuntu 14.11-0ubuntu0.22.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: banco; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE banco WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'es_GT.UTF-8';


ALTER DATABASE banco OWNER TO postgres;

\connect banco

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: crearcuenta(character varying); Type: PROCEDURE; Schema: public; Owner: postgres
--

CREATE PROCEDURE public.crearcuenta(IN p_cliente character varying)
    LANGUAGE plpgsql
    AS $$
	BEGIN
		INSERT INTO cuenta(cliente_id, es_activa, saldo)
		VALUES (p_cliente, true, 0.00);
	END;
$$;


ALTER PROCEDURE public.crearcuenta(IN p_cliente character varying) OWNER TO postgres;

--
-- Name: crearcuenta(integer, character varying); Type: PROCEDURE; Schema: public; Owner: postgres
--

CREATE PROCEDURE public.crearcuenta(IN p_cliente integer, IN p_moneda character varying)
    LANGUAGE plpgsql
    AS $$
	BEGIN
		INSERT INTO cuenta(cliente_id, es_activa, saldo, moneda)
		VALUES (p_cliente, true, 0.00, p_moneda);
	END;
$$;


ALTER PROCEDURE public.crearcuenta(IN p_cliente integer, IN p_moneda character varying) OWNER TO postgres;

--
-- Name: crearcuenta(character varying, character varying); Type: PROCEDURE; Schema: public; Owner: postgres
--

CREATE PROCEDURE public.crearcuenta(IN p_cliente character varying, IN p_moneda character varying)
    LANGUAGE plpgsql
    AS $$
	BEGIN
		INSERT INTO cuenta(cliente_id, es_activa, saldo, moneda)
		VALUES (p_cliente, true, 0.00, p_moneda);
	END;
$$;


ALTER PROCEDURE public.crearcuenta(IN p_cliente character varying, IN p_moneda character varying) OWNER TO postgres;

--
-- Name: crearempleado(integer, character varying, character varying); Type: PROCEDURE; Schema: public; Owner: postgres
--

CREATE PROCEDURE public.crearempleado(IN p_agencia integer, IN p_nombres character varying, IN p_apellidos character varying)
    LANGUAGE plpgsql
    AS $$
	BEGIN
		INSERT INTO empleado(agencia_id, nombres, apellidos) 
		VALUES (p_agencia, p_nombres, p_apellidos);

		UPDATE agencia SET no_empleados = no_empleados + 1 WHERE id =p_agencia;

	END;
$$;


ALTER PROCEDURE public.crearempleado(IN p_agencia integer, IN p_nombres character varying, IN p_apellidos character varying) OWNER TO postgres;

--
-- Name: notify_messenger_messages(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.notify_messenger_messages() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN
        PERFORM pg_notify('messenger_messages', NEW.queue_name::text);
        RETURN NEW;
    END;
$$;


ALTER FUNCTION public.notify_messenger_messages() OWNER TO postgres;

--
-- Name: transaccion(integer, integer, character varying, character varying, numeric, character varying); Type: PROCEDURE; Schema: public; Owner: postgres
--

CREATE PROCEDURE public.transaccion(IN p_cuenta integer, IN p_agencia integer, IN p_descripcion character varying, IN p_moneda character varying, IN p_monto numeric, IN p_tipo character varying)
    LANGUAGE plpgsql
    AS $_$
	DECLARE 
		saldo_cuenta NUMERIC; 		-- Saldo de la cuenta del cliente
		moneda_cuenta VARCHAR(1);	-- Tipo de moneda de la cuenta
		monto_convertido NUMERIC;  	-- Monto con el tipo de cambio aplicado
	BEGIN
		SELECT saldo INTO saldo_cuenta 
		FROM cuenta 
		WHERE id = p_cuenta;

		IF moneda_cuenta <> p_moneda THEN
        	IF p_moneda = 'Q' AND moneda_cuenta = '$' THEN
            	monto_convertido := p_monto / 8;
        	ELSIF p_moneda = '$' AND moneda_cuenta = 'Q' THEN
            	monto_convertido := p_monto * 8;
        	END IF;
		ELSE
			monto_convertido := p_monto;
    	END IF;

		IF p_tipo = 'R' THEN
			IF saldo_cuenta - monto_convertido < 0 THEN
				RAISE EXCEPTION 'ERROR: Fondos insuficientes en la cuenta.';
				RETURN;
			END IF;
		
			INSERT INTO transaccion (agencia_id, cuenta_id, descripcion, moneda, es_deposito, fecha, monto)
			VALUES (p_agencia, p_cuenta, p_descripcion, p_moneda, false, LOCALTIMESTAMP, p_monto);
			
			UPDATE cuenta SET saldo = saldo - monto_convertido WHERE id = p_cuenta;
		ELSEIF p_tipo = 'D' THEN
			INSERT INTO transaccion (agencia_id, cuenta_id, descripcion, moneda, es_deposito, fecha, monto)
			VALUES (p_agencia, p_cuenta, p_descripcion, p_moneda, true, LOCALTIMESTAMP, p_monto);
			
			UPDATE cuenta SET saldo = saldo + monto_convertido WHERE id = p_cuenta;
		END IF;
	END;
$_$;


ALTER PROCEDURE public.transaccion(IN p_cuenta integer, IN p_agencia integer, IN p_descripcion character varying, IN p_moneda character varying, IN p_monto numeric, IN p_tipo character varying) OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: agencia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.agencia (
    id integer NOT NULL,
    municipio_id integer,
    nombre character varying(25) NOT NULL,
    es_central boolean NOT NULL,
    no_empleados integer NOT NULL
);


ALTER TABLE public.agencia OWNER TO postgres;

--
-- Name: agencia_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.agencia_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agencia_id_seq OWNER TO postgres;

--
-- Name: agencia_id_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.agencia ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.agencia_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9
    CACHE 1
);


--
-- Name: cliente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cliente (
    id integer NOT NULL,
    municipio_id integer NOT NULL,
    documento_id integer NOT NULL,
    username character varying(180) NOT NULL,
    roles json NOT NULL,
    nombres character varying(30) DEFAULT NULL::character varying,
    apellidos character varying(30) DEFAULT NULL::character varying,
    razon character varying(50) DEFAULT NULL::character varying,
    password character varying(30) NOT NULL
);


ALTER TABLE public.cliente OWNER TO postgres;

--
-- Name: cliente_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cliente_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cliente_id_seq OWNER TO postgres;

--
-- Name: cliente_id_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.cliente ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.cliente_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9
    CACHE 1
);


--
-- Name: cuenta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cuenta (
    id integer NOT NULL,
    cliente_id integer NOT NULL,
    es_activa boolean NOT NULL,
    saldo integer NOT NULL,
    moneda character varying(1) NOT NULL
);


ALTER TABLE public.cuenta OWNER TO postgres;

--
-- Name: cuenta_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cuenta_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cuenta_id_seq OWNER TO postgres;

--
-- Name: cuenta_id_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.cuenta ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.cuenta_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9
    CACHE 1
);


--
-- Name: departamento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.departamento (
    id integer NOT NULL,
    nombre character varying(25) NOT NULL
);


ALTER TABLE public.departamento OWNER TO postgres;

--
-- Name: departamento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.departamento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.departamento_id_seq OWNER TO postgres;

--
-- Name: departamento_id_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.departamento ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.departamento_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9
    CACHE 1
);


--
-- Name: doctrine_migration_versions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.doctrine_migration_versions (
    version character varying(191) NOT NULL,
    executed_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    execution_time integer
);


ALTER TABLE public.doctrine_migration_versions OWNER TO postgres;

--
-- Name: documento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.documento (
    id integer NOT NULL,
    tipo character varying(10) NOT NULL
);


ALTER TABLE public.documento OWNER TO postgres;

--
-- Name: documento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.documento_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.documento_id_seq OWNER TO postgres;

--
-- Name: documento_id_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.documento ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.documento_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9
    CACHE 1
);


--
-- Name: empleado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.empleado (
    id integer NOT NULL,
    agencia_id integer NOT NULL,
    nombres character varying(30) NOT NULL,
    apellidos character varying(30) NOT NULL
);


ALTER TABLE public.empleado OWNER TO postgres;

--
-- Name: empleado_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.empleado_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.empleado_id_seq OWNER TO postgres;

--
-- Name: empleado_id_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.empleado ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.empleado_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9
    CACHE 1
);


--
-- Name: messenger_messages; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.messenger_messages (
    id bigint NOT NULL,
    body text NOT NULL,
    headers text NOT NULL,
    queue_name character varying(190) NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    available_at timestamp(0) without time zone NOT NULL,
    delivered_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone
);


ALTER TABLE public.messenger_messages OWNER TO postgres;

--
-- Name: COLUMN messenger_messages.created_at; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.messenger_messages.created_at IS '(DC2Type:datetime_immutable)';


--
-- Name: COLUMN messenger_messages.available_at; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.messenger_messages.available_at IS '(DC2Type:datetime_immutable)';


--
-- Name: COLUMN messenger_messages.delivered_at; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.messenger_messages.delivered_at IS '(DC2Type:datetime_immutable)';


--
-- Name: messenger_messages_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.messenger_messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.messenger_messages_id_seq OWNER TO postgres;

--
-- Name: messenger_messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.messenger_messages_id_seq OWNED BY public.messenger_messages.id;


--
-- Name: municipio; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.municipio (
    id integer NOT NULL,
    departamento_id integer NOT NULL,
    nombre character varying(25) NOT NULL
);


ALTER TABLE public.municipio OWNER TO postgres;

--
-- Name: municipio_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.municipio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.municipio_id_seq OWNER TO postgres;

--
-- Name: municipio_id_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.municipio ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.municipio_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9
    CACHE 1
);


--
-- Name: transaccion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.transaccion (
    id integer NOT NULL,
    agencia_id integer NOT NULL,
    cuenta_id integer NOT NULL,
    descripcion character varying(50) NOT NULL,
    moneda character varying(1) NOT NULL,
    monto integer NOT NULL,
    es_deposito boolean NOT NULL,
    fecha timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.transaccion OWNER TO postgres;

--
-- Name: transaccion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.transaccion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transaccion_id_seq OWNER TO postgres;

--
-- Name: transaccion_id_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.transaccion ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.transaccion_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 9
    CACHE 1
);


--
-- Name: messenger_messages id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.messenger_messages ALTER COLUMN id SET DEFAULT nextval('public.messenger_messages_id_seq'::regclass);


--
-- Name: agencia agencia_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.agencia
    ADD CONSTRAINT agencia_pkey PRIMARY KEY (id);


--
-- Name: cliente cliente_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT cliente_pkey PRIMARY KEY (id);


--
-- Name: cuenta cuenta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cuenta
    ADD CONSTRAINT cuenta_pkey PRIMARY KEY (id);


--
-- Name: departamento departamento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.departamento
    ADD CONSTRAINT departamento_pkey PRIMARY KEY (id);


--
-- Name: doctrine_migration_versions doctrine_migration_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.doctrine_migration_versions
    ADD CONSTRAINT doctrine_migration_versions_pkey PRIMARY KEY (version);


--
-- Name: documento documento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.documento
    ADD CONSTRAINT documento_pkey PRIMARY KEY (id);


--
-- Name: empleado empleado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.empleado
    ADD CONSTRAINT empleado_pkey PRIMARY KEY (id);


--
-- Name: messenger_messages messenger_messages_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.messenger_messages
    ADD CONSTRAINT messenger_messages_pkey PRIMARY KEY (id);


--
-- Name: municipio municipio_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.municipio
    ADD CONSTRAINT municipio_pkey PRIMARY KEY (id);


--
-- Name: transaccion transaccion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transaccion
    ADD CONSTRAINT transaccion_pkey PRIMARY KEY (id);


--
-- Name: idx_31c7bfcfde734e51; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_31c7bfcfde734e51 ON public.cuenta USING btree (cliente_id);


--
-- Name: idx_75ea56e016ba31db; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_75ea56e016ba31db ON public.messenger_messages USING btree (delivered_at);


--
-- Name: idx_75ea56e0e3bd61ce; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_75ea56e0e3bd61ce ON public.messenger_messages USING btree (available_at);


--
-- Name: idx_75ea56e0fb7336f0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_75ea56e0fb7336f0 ON public.messenger_messages USING btree (queue_name);


--
-- Name: idx_bff96af79aeff118; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_bff96af79aeff118 ON public.transaccion USING btree (cuenta_id);


--
-- Name: idx_bff96af7a6f796be; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_bff96af7a6f796be ON public.transaccion USING btree (agencia_id);


--
-- Name: idx_d9d9bf52a6f796be; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_d9d9bf52a6f796be ON public.empleado USING btree (agencia_id);


--
-- Name: idx_eb6c2b9958bc1be0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_eb6c2b9958bc1be0 ON public.agencia USING btree (municipio_id);


--
-- Name: idx_f41c9b2545c0cf75; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_f41c9b2545c0cf75 ON public.cliente USING btree (documento_id);


--
-- Name: idx_f41c9b2558bc1be0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_f41c9b2558bc1be0 ON public.cliente USING btree (municipio_id);


--
-- Name: idx_fe98f5e05a91c08d; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX idx_fe98f5e05a91c08d ON public.municipio USING btree (departamento_id);


--
-- Name: uniq_identifier_username; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX uniq_identifier_username ON public.cliente USING btree (username);


--
-- Name: messenger_messages notify_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER notify_trigger AFTER INSERT OR UPDATE ON public.messenger_messages FOR EACH ROW EXECUTE FUNCTION public.notify_messenger_messages();


--
-- Name: cuenta fk_31c7bfcfde734e51; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cuenta
    ADD CONSTRAINT fk_31c7bfcfde734e51 FOREIGN KEY (cliente_id) REFERENCES public.cliente(id);


--
-- Name: transaccion fk_bff96af79aeff118; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transaccion
    ADD CONSTRAINT fk_bff96af79aeff118 FOREIGN KEY (cuenta_id) REFERENCES public.cuenta(id);


--
-- Name: transaccion fk_bff96af7a6f796be; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transaccion
    ADD CONSTRAINT fk_bff96af7a6f796be FOREIGN KEY (agencia_id) REFERENCES public.agencia(id);


--
-- Name: empleado fk_d9d9bf52a6f796be; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.empleado
    ADD CONSTRAINT fk_d9d9bf52a6f796be FOREIGN KEY (agencia_id) REFERENCES public.agencia(id);


--
-- Name: agencia fk_eb6c2b9958bc1be0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.agencia
    ADD CONSTRAINT fk_eb6c2b9958bc1be0 FOREIGN KEY (municipio_id) REFERENCES public.municipio(id);


--
-- Name: cliente fk_f41c9b2545c0cf75; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT fk_f41c9b2545c0cf75 FOREIGN KEY (documento_id) REFERENCES public.documento(id);


--
-- Name: cliente fk_f41c9b2558bc1be0; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT fk_f41c9b2558bc1be0 FOREIGN KEY (municipio_id) REFERENCES public.municipio(id);


--
-- Name: municipio fk_fe98f5e05a91c08d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.municipio
    ADD CONSTRAINT fk_fe98f5e05a91c08d FOREIGN KEY (departamento_id) REFERENCES public.departamento(id);


--
-- PostgreSQL database dump complete
--

