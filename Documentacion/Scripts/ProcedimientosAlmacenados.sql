CREATE OR REPLACE PROCEDURE crearCuenta(
	p_cliente INTEGER, -- ID del cliente de la cuenta
	p_moneda VARCHAR(1)	-- Caracter de la moneda de la cuenta
)
AS 
$$
	BEGIN
		INSERT INTO cuenta(cliente_id, es_activa, saldo, moneda)
		VALUES (p_cliente, true, 0.00, p_moneda);
	END;
$$
LANGUAGE plpgsql

CREATE OR REPLACE PROCEDURE transaccion(
	p_cuenta INTEGER,		-- ID de la cuenta propietaria
	p_agencia INTEGER, 		 -- ID de la agencia
	p_descripcion VARCHAR(50), -- Descripcion de la transaccion
	p_moneda VARCHAR(1), 	  -- Caracter de la moneda (Q o $)
	p_monto NUMERIC, 		  -- Cantidad
	p_tipo VARCHAR(1)		  -- Indica si es deposito o retiro (D o R)
)
AS 
$$
	DECLARE 
		saldo_cuenta NUMERIC; 		-- Saldo de la cuenta del cliente
		moneda_cuenta VARCHAR(1);	-- Tipo de moneda de la cuenta
		monto_convertido NUMERIC;  	-- Monto con el tipo de cambio aplicado
	BEGIN
		SELECT saldo INTO saldo_cuenta 
		FROM cuenta 
		WHERE id = p_cuenta;

		IF moneda_cuenta <> p_moneda THEN
        	IF p_moneda = 'Q' AND moneda_cuenta = '$' THEN
            	monto_convertido := p_monto / 8;
        	ELSIF p_moneda = '$' AND moneda_cuenta = 'Q' THEN
            	monto_convertido := p_monto * 8;
        	END IF;
		ELSE
			monto_convertido := p_monto;
    	END IF;

		IF p_tipo = 'R' THEN
			IF saldo_cuenta - monto_convertido < 0 THEN
				RAISE EXCEPTION 'ERROR: Fondos insuficientes en la cuenta.';
				RETURN;
			END IF;
		
			INSERT INTO transaccion (agencia_id, cuenta_id, descripcion, moneda, es_deposito, fecha, monto)
			VALUES (p_agencia, p_cuenta, p_descripcion, p_moneda, false, LOCALTIMESTAMP, p_monto);
			
			UPDATE cuenta SET saldo = saldo - monto_convertido WHERE id = p_cuenta;
		ELSEIF p_tipo = 'D' THEN
			INSERT INTO transaccion (agencia_id, cuenta_id, descripcion, moneda, es_deposito, fecha, monto)
			VALUES (p_agencia, p_cuenta, p_descripcion, p_moneda, true, LOCALTIMESTAMP, p_monto);
			
			UPDATE cuenta SET saldo = saldo + monto_convertido WHERE id = p_cuenta;
		END IF;
	END;
$$
LANGUAGE plpgsql

CREATE OR REPLACE PROCEDURE crearEmpleado (
	p_agencia INTEGER,	--ID de la agencia
	p_nombres VARCHAR(30), -- Nombre del empleado
	p_apellidos VARCHAR(30) -- Apellidos del empleado
)
AS
$$
	BEGIN
		INSERT INTO empleado(agencia_id, nombres, apellidos) 
		VALUES (p_agencia, p_nombres, p_apellidos);

		UPDATE agencia SET no_empleados = no_empleados + 1 WHERE id =p_agencia;

	END;
$$
LANGUAGE plpgsql
